#include <iostream>
#include <string>

int main () {
    using namespace std;
    string number1;
    string number2;
    cout << "State Your First Number" << endl;
    getline (cin, number1);
    cout << "State Your Second Number" << endl;
    getline (cin, number2);
    
    int a = stol(number1);
    int b = stol(number2);
    int output1 = a + b;
    int output2 = a * b;
    int output3 = a - b;
    cout << "Your First Number +, * or - Your Second Number = " << endl;
    cout << output1 << endl;
    cout << output2 << endl;
    cout << output3 << endl;
}
